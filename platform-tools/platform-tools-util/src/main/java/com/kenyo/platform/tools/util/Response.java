package com.kenyo.platform.tools.util;

import java.util.ArrayList;
import java.util.List;

public class Response<T> {

	public String message = "OK";
	public boolean state = false;
	public List<T> data;
	
	public Response() {
		this.data = new ArrayList<>();
	}
}
