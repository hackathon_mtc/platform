package com.kenyo.platform.tools.util;

public enum ERole {
	
	ADMIN(1L, "ADMIN"),
	USER(2L, "USER"),
	CUSTOMER(3L, "CUSTOMER");
	
	public String SLUG;
	public Long ID;
	
	private ERole(Long id, String SLUG) {
		this.ID = id;
		this.SLUG = SLUG;
	}

}
