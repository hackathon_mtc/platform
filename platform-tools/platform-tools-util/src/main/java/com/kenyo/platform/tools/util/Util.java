package com.kenyo.platform.tools.util;

public class Util {

	public final static String FORMATO_FECHA_FRONT = "dd/MM/yyyy HH:mm:ss";
	
	/**
	 * 
	 * @param  latitud(X)
	 * @param 	longitud (Y)
	 * @return
	 */
	public static String toWKT(double longitud, double latitud) {
		return "POINT (" + latitud + " " + longitud  + ")";
	}
}
