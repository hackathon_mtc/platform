package com.kenyo.platform.modules.dao;

import com.kenyo.platform.modules.model.Alert;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IAlertDAO extends JpaRepository<Alert, Long> {

	@Query("from Alert a")
	Page<Alert> findAlerts(Pageable pageable);
	
	@Query(value = "select * from public.mtc_closeto_bomberos(?1, ?2, ?3)", nativeQuery = true)
	List<Object[]> getBomberos(double latitud, double longitud, int cantidad);
	
	@Query(value = "select * from public.mtc_closeto_comisarias(?1, ?2, ?3)", nativeQuery = true)
	List<Object[]> getComisarias(double latitud, double longitud, int cantidad);
	
	@Query(value = "select * from public.mtc_closeto_hospitales(?1, ?2, ?3)", nativeQuery = true)
	List<Object[]> getHospitales(double latitud, double longitud, int cantidad);
}
