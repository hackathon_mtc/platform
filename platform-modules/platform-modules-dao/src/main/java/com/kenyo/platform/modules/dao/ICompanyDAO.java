package com.kenyo.platform.modules.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.kenyo.platform.modules.model.Company;

public interface ICompanyDAO extends JpaRepository<Company, Long> {

	@Query("from Company c where 1=1 OR LOWER(c.name)  LIKE LOWER(CONCAT('%',:searchterm, '%' ))")
	Page<Company> findCompanies(@Param("searchterm") String searchterm, Pageable pageable);
	
	
}
