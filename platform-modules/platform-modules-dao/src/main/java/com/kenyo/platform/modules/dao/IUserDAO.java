package com.kenyo.platform.modules.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.kenyo.platform.modules.model.User;

public interface IUserDAO extends JpaRepository<User, Long> {

	User findOneByEmail(String email);
	
	@Query("select u from User u INNER JOIN u.company c where LOWER(u.name) LIKE LOWER(CONCAT('%',:searchterm, '%' )) OR "
			+ " LOWER(u.lastname) LIKE LOWER(CONCAT('%',:searchterm, '%' )) OR "
			+ " LOWER(u.email) LIKE  LOWER(CONCAT('%',:searchterm, '%' )) OR "
			+ " LOWER(u.dni) LIKE  LOWER(CONCAT('%',:searchterm, '%' )) OR "
			+ " LOWER(c.name) LIKE  LOWER(CONCAT('%',:searchterm, '%' ))")
	Page<User> findUsers(@Param("searchterm") String searchterm, Pageable pageable);
	
	
	@Query("from User u where u.company.id =:company ")
	Page<User> findUsersByCompany(@Param("company") Long company, Pageable pageable);
	
	@Query("from User u where u.company.id =:company ")
	List<User> findUsersByCompany(@Param("company") Long company);
}
