package com.kenyo.platform.modules.service;

import java.util.Locale;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.kenyo.platform.modules.model.User;
import com.kenyo.platform.tools.util.Response;

public interface ICRUD<T> {
	
	Response<T> create(T object, User usercreate, Locale locale);
	
	Response<T> update(T object, User usercreate, Locale locale);
	
	Response<T> delete(Long id, Locale locale);
	
	Response<Page<T>> findObjects(String searchterm, Pageable pageable, Locale locale);

}
