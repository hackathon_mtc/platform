package com.kenyo.platform.modules.service.impl;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import com.kenyo.platform.modules.dao.IUserDAO;
import com.kenyo.platform.modules.model.Alert;
import com.kenyo.platform.modules.model.Company;
import com.kenyo.platform.modules.model.User;
import com.kenyo.platform.tools.util.EMail;


@Component
//@PropertySource({ "classpath:application.properties" })
public class EmailService {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    private JavaMailSender emailSender;
	
	@Autowired
    private SpringTemplateEngine templateEngine;

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private IUserDAO userDao;
	
	@Value("${mtc.front}")
	private String url;
	
	public boolean sendAlert(Company company, Alert alert, Locale locale) {
		List<User> users = userDao.findUsersByCompany(company.getId());
		users.stream().forEach(user -> {
			EMail mail = new EMail();
			mail.setFrom("test@gmail.com");
			mail.setTo(user.getEmail());
			mail.setSubject("Emisión de alerta");
			mail.getModel().put("user", user);
			mail.getModel().put("alerta", alert);
			mail.getModel().put("url", url + "/" + alert.getId());
			Object[] parametros = new Object[] {alert.getId()};
			//mail.getModel().put("url", MessageFormat.format(url, parametros));
			enviarNotificacion(mail, "notificacion");
		});
		return true;
	}
	
	private boolean enviarNotificacion(EMail mail, String template) {

		boolean respuesta = false;
		try {
			 MimeMessage message = emailSender.createMimeMessage();
	         MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
	         Context context = new Context();
	         context.setVariables(mail.getModel());
	         String html = templateEngine.process(template, context);
	         helper.setTo(mail.getTo());	
	         if(mail.getCopies() != null && mail.getCopies().length > 0) {
	        	 helper.setCc(mail.getCopies()); 
	         }
	         helper.setText(html, true);
	         helper.setSubject(mail.getSubject());
	         helper.setFrom(mail.getFrom());		
	         emailSender.send(message);
	         respuesta = true;
		} catch (Exception e) {
			respuesta = false;
			LOGGER.error("[ERROR] enviarNotificacion", e);
		}
		LOGGER.info("[FIN] enviarNotificacion " + respuesta);
		return respuesta;
	}

}
