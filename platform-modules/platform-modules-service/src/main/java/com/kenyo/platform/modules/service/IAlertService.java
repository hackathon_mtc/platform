package com.kenyo.platform.modules.service;

import java.util.List;
import java.util.Locale;

import com.kenyo.platform.modules.model.Alert;
import com.kenyo.platform.modules.service.dto.PlaceCloseDto;
import com.kenyo.platform.tools.util.Response;


public interface IAlertService extends ICRUD<Alert>{
	
	List<PlaceCloseDto> getComisarias(double lat, double longi, int cantidad);

	List<PlaceCloseDto> getBomberos(double lat, double longi, int cantidad);
	
	List<PlaceCloseDto> getHospitales(double lat, double longi, int cantidad);

	Response<Alert> getAlert(Long id, Locale Locale);
}
