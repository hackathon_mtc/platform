package com.kenyo.platform.modules.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.kenyo.platform.modules.dao.IAlertDAO;
import com.kenyo.platform.modules.model.Alert;
import com.kenyo.platform.modules.model.User;
import com.kenyo.platform.modules.service.IAlertService;
import com.kenyo.platform.modules.service.dto.PlaceCloseDto;
import com.kenyo.platform.tools.util.Response;

@Service
public class AlertServiceImpl implements IAlertService{
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private IAlertDAO alerDAO;

	@Override
	public Response<Alert> create(Alert object, User usercreate, Locale locale) {
		Response<Alert> response = new Response<Alert>();
		try {
			object.setDateCreate(new Date());
			object.setUserCreate(usercreate.getId());
			object = alerDAO.save(object);
			if(object == null)
				throw new Exception(messageSource.getMessage("mtc.alert.register.error", null, locale));
			response.state = true;
			response.message = messageSource.getMessage("mtc.alert.register.ok", null, locale);
			response.data.add(object);
		} catch (Exception e) {
			response.state = false;
			response.message = e.getMessage();
		}
		
		return response;
	}

	@Override
	public Response<Alert> update(Alert object, User usercreate, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<Alert> delete(Long id, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<Page<Alert>> findObjects(String searchterm, Pageable pageable, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PlaceCloseDto> getComisarias(double lat, double longi, int cantidad) {
		List<PlaceCloseDto> list = new ArrayList<>();
		alerDAO.getComisarias(longi, lat, cantidad)
				.forEach(place -> {
					PlaceCloseDto p = new PlaceCloseDto();
					p.setCorreo(String.valueOf(place[0]));
					p.setDireccion(String.valueOf(place[1]));
					p.setResponsable(String.valueOf(place[2]));
					p.setNombre(String.valueOf(place[3]));
					p.setTelefono(String.valueOf(place[4]));
					p.setLongitud(Double.valueOf(place[6].toString()));
					p.setLatitud(Double.valueOf(place[5].toString()));
					p.setTipoLugar("COMISARIA");
					list.add(p);
				});
		return list;
	}

	@Override
	public List<PlaceCloseDto> getBomberos(double lat, double longi, int cantidad) {
		List<PlaceCloseDto> list = new ArrayList<>();
		alerDAO.getBomberos(longi, lat, cantidad)
				.forEach(place -> {
					PlaceCloseDto p = new PlaceCloseDto();
					p.setCorreo(String.valueOf(place[0]));
					p.setDireccion(String.valueOf(place[1]));
					p.setResponsable(String.valueOf(place[2]));
					p.setNombre(String.valueOf(place[3]));
					p.setTelefono(String.valueOf(place[4]));
					p.setLongitud(Double.valueOf(place[6].toString()));
					p.setLatitud(Double.valueOf(place[5].toString()));
					p.setTipoLugar("BOMBEROS");
					list.add(p);
				});
		return list;
	}

	@Override
	public Response<Alert> getAlert(Long id, Locale Locale) {
		Response<Alert> response = new Response<Alert>();
		try {
			Optional<Alert> opt= alerDAO.findById(id);
			if(!opt.isPresent())
				throw new Exception("NO eneocntrado");
			response.state = true;
			response.message = "OK";
			response.data.add(opt.get());
		} catch (Exception e) {
			response.state = false;
			response.message = e.getMessage();
		}
		return response;
	}

	@Override
	public List<PlaceCloseDto> getHospitales(double lat, double longi, int cantidad) {
		List<PlaceCloseDto> list = new ArrayList<>();
		alerDAO.getHospitales(longi, lat, cantidad)
				.forEach(place -> {
					PlaceCloseDto p = new PlaceCloseDto();
					p.setCorreo(String.valueOf(place[0]));
					p.setDireccion(String.valueOf(place[1]));
					p.setResponsable(String.valueOf(place[2]));
					p.setNombre(String.valueOf(place[3]));
					p.setTelefono(String.valueOf(place[4]));
					p.setLongitud(Double.valueOf(place[6].toString()));
					p.setLatitud(Double.valueOf(place[5].toString()));
					p.setTipoLugar("BOMBEROS");
					list.add(p);
				});
		return list;
	}
	
	

}
