package com.kenyo.platform.modules.service;

import java.util.Locale;

import com.kenyo.platform.modules.model.Alert;
import com.kenyo.platform.modules.model.Company;
import com.kenyo.platform.modules.service.dto.PlaceCloseDto;
import com.kenyo.platform.modules.service.dto.RequesAletDto;
import com.kenyo.platform.tools.util.Response;

public interface INotifyService {
	
	Response<Alert> registrarNotificacion(RequesAletDto request, Company company, Locale locale);

	Response<Alert> getAlert(Long id, Locale locale);
	
	Response<PlaceCloseDto> comisariasCercanas(Long id);
	
	Response<PlaceCloseDto> bomberosCercanas(Long id);
	
	Response<PlaceCloseDto> HospitalesCercanas(Long id);
}
