package com.kenyo.platform.modules.service.impl;

import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.kenyo.platform.modules.dao.IUserDAO;
import com.kenyo.platform.modules.model.User;
import com.kenyo.platform.modules.service.IUserService;
import com.kenyo.platform.tools.util.Response;

@Service
public class UserMServiceImpl implements IUserService {

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private IUserDAO userDAO;
	
	@Override
	public Response<User> create(User object, User usercreate, Locale locale) {
		Response<User> response = new Response<User>();
		try {
			object.setUserCreate(usercreate == null ? 1L : usercreate.getId());
			object.setDateCreate(new Date());
			object = userDAO.save(object);
			if(object == null)
					throw new Exception(messageSource.getMessage("mtc.user.register.error", null, locale));
				response.state = true;
				response.message = messageSource.getMessage("mtc.user.register.ok", null, locale);
				response.data.add(object);
		} catch (Exception e) {
			response.state = false;
			response.message = e.getMessage();
		}
		return response;
	}

	@Override
	public Response<User> update(User object, User usercreate, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<User> delete(Long id, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<Page<User>> findObjects(String searchterm, Pageable pageable, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

}
