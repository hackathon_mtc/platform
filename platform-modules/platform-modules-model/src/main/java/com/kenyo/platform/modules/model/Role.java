package com.kenyo.platform.modules.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rol")
public class Role extends EntityBase{

	@Id
	private Long id;
	
	@Column(nullable = false, name = "nombre")
	private String name;
	
	@Column(nullable = false, name = "slug")
	private String slug;
	
	public Role() {}
	
	
	

	public Role(Long id) {
		super();
		this.id = id;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}
	
	
}
