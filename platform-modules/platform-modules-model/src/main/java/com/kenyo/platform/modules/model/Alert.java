package com.kenyo.platform.modules.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kenyo.platform.tools.util.Util;
import com.vividsolutions.jts.geom.Point;

@Entity
@Table(name = "alerta")
public class Alert extends EntityBase {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, name = "mensaje", length = 160)
	private String message;
	
	@ManyToOne
	@JoinColumn(name = "usuario_reporta", nullable = false)
	private User userfrom;
	
	@Column(nullable = false, name = "estado", length = 20)
	private String state;
	

	@ManyToOne
	@JoinColumn(name = "usuario_responsable", nullable = true)
	private User userresponsable;
	
	@Column(nullable = false)
	private Double latitud;
	
	@Column(nullable = false)
	private Double longitud;
	
	@Column(nullable = true, name = "descripcion_lugar")
	private String place;
	
	@Column(nullable = true, name = "tipo_alerta")
	private String type;
	
	@JsonIgnore
	private Point geometria;
	
	@Transient
	private String wkt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getUserfrom() {
		return userfrom;
	}

	public void setUserfrom(User userfrom) {
		this.userfrom = userfrom;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public User getUserresponsable() {
		return userresponsable;
	}

	public void setUserresponsable(User userresponsable) {
		this.userresponsable = userresponsable;
	}

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	public Double getLongitud() {
		return longitud;
	}

	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Point getGeometria() {
		return geometria;
	}

	public void setGeometria(Point geometria) {
		this.geometria = geometria;
	}

	public String getWkt() {
		this.wkt = Util.toWKT(longitud, latitud);
		return wkt;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
}
