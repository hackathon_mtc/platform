package com.kenyo.platform.modules.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kenyo.platform.tools.util.Util;

@MappedSuperclass
public class EntityBase {
	
	@Column(nullable = false, name = "usuario_creacion")
	private Long userCreate;
	
	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Util.FORMATO_FECHA_FRONT, timezone = "America/Bogota")
	@Column(nullable = false, name = "fecha_creacion")
	private Date dateCreate;
	
	@Column(nullable = true, name = "usuario_modificacion")
	private Long userUpdate;
	
	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Util.FORMATO_FECHA_FRONT, timezone = "America/Bogota")
	@Column(nullable = true, name = "fecha_actualizacion")
	private Date dateUpdate;

	public Long getUserCreate() {
		return userCreate;
	}

	public void setUserCreate(Long userCreate) {
		this.userCreate = userCreate;
	}

	public Date getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Long getUserUpdate() {
		return userUpdate;
	}

	public void setUserUpdate(Long userUpdate) {
		this.userUpdate = userUpdate;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

}
