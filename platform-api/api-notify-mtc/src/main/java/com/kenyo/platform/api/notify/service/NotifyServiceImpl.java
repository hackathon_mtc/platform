package com.kenyo.platform.api.notify.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.kenyo.platform.modules.dao.IAlertDAO;
import com.kenyo.platform.modules.dao.IUserDAO;
import com.kenyo.platform.modules.model.Alert;
import com.kenyo.platform.modules.model.Company;
import com.kenyo.platform.modules.model.Role;
import com.kenyo.platform.modules.model.User;
import com.kenyo.platform.modules.service.IAlertService;
import com.kenyo.platform.modules.service.INotifyService;
import com.kenyo.platform.modules.service.IUserService;
import com.kenyo.platform.modules.service.dto.PlaceCloseDto;
import com.kenyo.platform.modules.service.dto.RequesAletDto;
import com.kenyo.platform.modules.service.impl.EmailService;
import com.kenyo.platform.tools.util.ERole;
import com.kenyo.platform.tools.util.Response;
import com.kenyo.platform.tools.util.Util;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

@Service
public class NotifyServiceImpl implements INotifyService {
	
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IAlertService alertService;
	
	@Autowired
	private EmailService emailSerice;
	

	@Override
	public Response<Alert> registrarNotificacion(RequesAletDto request, Company company, Locale locale) {
		Response<Alert> response = new Response<Alert>();
		try {
			User user = new User();
			user.setCompany(null);
			user.setDni(request.getDni());
			user.setEmail(request.getEmail() == null ? "kenyojoel903@gmail.com" : request.getEmail() );
			user.setEnabled(false);
			user.setLastname("ciudadano");
			user.setName("ciudaddano");
			user.setPhonenumber(request.getPhonenumber());
			user.setPassword("123456");
			Role role  = new Role(ERole.CUSTOMER.ID);
			List<Role> roles = new ArrayList<Role>();
			roles.add(role);
			user.setRoles(roles);
			Response<User> resp = userService.create(user, null, locale);
			if(resp.state == false)
				throw new Exception(resp.message);
			Alert alert = new Alert();
			alert.setLatitud(request.getLatitud());
			alert.setLongitud(request.getLongitud());
			alert.setGeometria((Point) wktToGeometry(Util.toWKT(request.getLongitud(), request.getLatitud())));
			alert.setMessage(request.getMessage());
			alert.setPlace("No se sabe");
			alert.setState("NO_ATENDIDO");
			alert.setUserfrom(resp.data.get(0));
			alert.setType(request.getAlertType());
			response = alertService.create(alert, resp.data.get(0), locale);
			if(response.state == true) {
				emailSerice.sendAlert(company, alert, locale);
			}
		} catch (Exception e) {
			response.state = false;
			response.message = e.getMessage();
		}
		return response;
	}
	
	private Geometry wktToGeometry(String wktPoint) {
		WKTReader fromText = new WKTReader();
		Geometry geom = null;
		try {
			geom = fromText.read(wktPoint);
			geom.setSRID(4326);
		} catch (ParseException e) {
			throw new RuntimeException("Not a WKT string:" + wktPoint);
		}
		return geom;
	}

	@Override
	public Response<Alert> getAlert(Long id, Locale locale) {
		return alertService.getAlert(id, locale);
	}

	@Override
	public Response<PlaceCloseDto> comisariasCercanas(Long id) {
		Response<PlaceCloseDto> response = new Response<PlaceCloseDto>();
		try {
			Response<Alert> res = alertService.getAlert(id, null);
			if(res.state == false) {
				response.data.addAll(new ArrayList<PlaceCloseDto>());
			} else {
				Alert alert = res.data.get(0);
				LOGGER.info("aaaa: " + alert.getLatitud() +"  "+alert.getLongitud());
				response.data.addAll(alertService.getComisarias(alert.getLatitud(), alert.getLongitud(), 4));
			}
			response.state = true;
			response.message = "OK";
		} catch (Exception e) {
			response.state = false;
			response.message = e.getMessage();
		}
		return response;
	}

	@Override
	public Response<PlaceCloseDto> bomberosCercanas(Long id) {
		Response<PlaceCloseDto> response = new Response<PlaceCloseDto>();
		try {
			Response<Alert> res = alertService.getAlert(id, null);
			if(res.state == false) {
				response.data.addAll(new ArrayList<PlaceCloseDto>());
			} else {
				Alert alert = res.data.get(0);
				response.data.addAll(alertService.getBomberos(alert.getLatitud(), alert.getLongitud(), 4));
			}
			response.state = true;
			response.message = "OK";
		} catch (Exception e) {
			response.state = false;
			response.message = e.getMessage();
		}
		return response;
	}

	@Override
	public Response<PlaceCloseDto> HospitalesCercanas(Long id) {
		Response<PlaceCloseDto> response = new Response<PlaceCloseDto>();
		try {
			Response<Alert> res = alertService.getAlert(id, null);
			if(res.state == false) {
				response.data.addAll(new ArrayList<PlaceCloseDto>());
			} else {
				Alert alert = res.data.get(0);
				response.data.addAll(alertService.getHospitales(alert.getLatitud(), alert.getLongitud(), 4));
			}
			response.state = true;
			response.message = "OK";
		} catch (Exception e) {
			response.state = false;
			response.message = e.getMessage();
		}
		return response;
	}

	
}
