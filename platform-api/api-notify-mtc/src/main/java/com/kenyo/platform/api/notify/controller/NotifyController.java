package com.kenyo.platform.api.notify.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kenyo.platform.modules.model.Alert;
import com.kenyo.platform.modules.model.Company;
import com.kenyo.platform.modules.service.INotifyService;
import com.kenyo.platform.modules.service.dto.PlaceCloseDto;
import com.kenyo.platform.modules.service.dto.RequesAletDto;
import com.kenyo.platform.tools.util.Response;

@RestController
@RequestMapping("/api/notify")
public class NotifyController {
	
	@Autowired
	private INotifyService notifyService;
	
	@Value("mtc.company.id")
	private String idCompayMtc;
	
	@GetMapping(value = "/v1", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Alert>> createAlert(
			@RequestParam String message, 
			@RequestParam String email,
			@RequestParam String dni,
			@RequestParam String aletType,
			@RequestParam String phonenumber,
			@RequestParam double latitud,
			@RequestParam double longitud, Locale locale){
		RequesAletDto request = new RequesAletDto();
		request.setAlertType(aletType);
		request.setDni(dni);
		request.setEmail(email);
		request.setLatitud(latitud);
		request.setLongitud(longitud);
		request.setMessage(message);
		request.setPhonenumber(phonenumber);
		Company company = new Company();
		company.setId(2L);
		Response<Alert> response = notifyService.registrarNotificacion(request, company, locale);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Response<Alert>> getAlert(@PathVariable("id") Long id, Locale locale){
		Response<Alert> response = notifyService.getAlert(id, locale);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/comisarias/{id}")
	public ResponseEntity<Response<PlaceCloseDto>> getComisarias(@PathVariable("id") Long id, Locale locale){
		Response<PlaceCloseDto> response = notifyService.comisariasCercanas(id);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/bomberos/{id}")
	public ResponseEntity<Response<PlaceCloseDto>> getBomberos(@PathVariable("id") Long id, Locale locale){
		Response<PlaceCloseDto> response = notifyService.bomberosCercanas(id);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/hospitales/{id}")
	public ResponseEntity<Response<PlaceCloseDto>> getHospitales(@PathVariable("id") Long id, Locale locale){
		Response<PlaceCloseDto> response = notifyService.HospitalesCercanas(id);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
