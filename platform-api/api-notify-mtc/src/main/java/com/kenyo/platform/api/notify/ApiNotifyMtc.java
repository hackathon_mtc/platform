package com.kenyo.platform.api.notify;

import java.nio.charset.StandardCharsets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "com.kenyo.platform"})
@EntityScan(basePackages = {"com.kenyo.platform.modules.model"})
@EnableJpaRepositories(basePackages = "com.kenyo.platform.modules.dao")
@PropertySource({ "classpath:application.properties" })
public class ApiNotifyMtc {
	
	public static void main(String[] args) {
		SpringApplication.run(ApiNotifyMtc.class, args);
	}
	
	
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages");
		messageSource.setDefaultEncoding(StandardCharsets.ISO_8859_1.name());
		return messageSource;
	}

}
